# EKS Stackdriver Logging

Reference for setting up stackdriver logging on existing clusters:
https://kubernetes.io/docs/tasks/debug-application-cluster/logging-stackdriver/

## Configuration
The fluentd google_cloud output plugin expects some instance metadata. These metadata values are not present on non-GKE clusters so you need to set them here. You can likely retrieve these from EC2 environment variables.

[fluentd-config.yaml > data.fluent.conf](fluentd-config.yaml)
``` ruby
<match **>
  @type copy
  <store>
    # Stackdriver output
    @type google_cloud
    @id out_google_cloud

    vm_id MY_VM_ID # EC2 Instance Id (TODO: get from EC2 environment vars)
    zone MY_ZONE # AWS Region (TODO: get from EC2 environment vars)
    project_id MY_PROJECT_ID # Google project ID
  </store>
</match>
```

### Authorization:
Create log writer service account: https://cloud.google.com/logging/docs/agent/authorization
Download `credentials.json` file for logging service account

Create config map containing credentials file:
```bash
kubectl create configmap fluentd-gcp-creds-config --from-file=<credentials_file>
```

## Build
Build the fluentd-agent container:
``` bash
# fluentd-agent/
docker build -t <image_name>:<image_tag> .
docker push <image_name>:<image_tag>
```

Update the docker image in [fluentd-stackdriver-ds.yaml > spec.template.spec](fluentd-stackdriver-ds.yaml) with your new `<image_name>:<image_tag>`

## Deploy
Deploy daemonset:
``` bash
kubectl apply -f .
```
